document.addEventListener("DOMContentLoaded", function() {

    var btnBuscar = document.getElementById("Buscar");
    var txtBuscar = document.getElementById("txtBuscar");
    var lista = document.getElementById("lista");


    btnBuscar.addEventListener("click", function() {
  
        var idBuscar = txtBuscar.value;


        consultarTituloPorId(idBuscar);
    });


    var btnCargar = document.getElementById("btnCargar");
    btnCargar.addEventListener("click", function() {

        hacerpeticion();
    });


    var btnLimpiar = document.getElementById("btnLimpiar");
    btnLimpiar.addEventListener("click", function() {
  
        lista.innerHTML = "";
 
        txtBuscar.value = "";
    });


    function hacerpeticion() {
        const http = new XMLHttpRequest();
        const url = "https://jsonplaceholder.typicode.com/albums";

    
        http.onreadystatechange = function() {
            if (this.status == 200 && this.readyState == 4) {
               
                let res = document.getElementById("lista");
                const json = JSON.parse(this.responseText);

             
                for (const datos of json) {
                    res.innerHTML += '<tr> <td class="columna1">' + datos.userId + '</td>' +
                        '<td class="columna2">' + datos.id + '</td>' +
                        '<td class="columna3">' + datos.title + '</td> </tr>';
                }
                res.innerHTML += "</tbody>";
            }
        };
        http.open('GET', url, true);
        http.send();
    }


    function consultarTituloPorId(id) {
        const http = new XMLHttpRequest();
        const url = "https://jsonplaceholder.typicode.com/albums/" + id;


        http.onreadystatechange = function() {
            if (this.status == 200 && this.readyState == 4) {
                
                let res = document.getElementById("lista");
                res.innerHTML = ""; 
                const datos = JSON.parse(this.responseText);
                res.innerHTML += '<tr> <td class="columna1">' + datos.userId + '</td>' +
                    '<td class="columna2">' + datos.id + '</td>' +
                    '<td class="columna3">' + datos.title + '</td> </tr>';
            } else if (this.status == 404) {
      
                alert("ID no encontrado");
            }
        };
        http.open('GET', url, true);
        http.send();
    }
});

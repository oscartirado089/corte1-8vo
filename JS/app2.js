function hacerpeticionPorId(id) {
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/users/" + id;


    http.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {


            let res = document.getElementById("lista");
            res.innerHTML = "";

            const datos = JSON.parse(this.responseText);

            if (datos.hasOwnProperty('id')) {

                let fila = document.createElement("tr");
                fila.innerHTML += '<td class="columna1">' + datos.id + '</td>'
                    + '<td class="columna2">' + datos.name + '</td>'
                    + '<td class="columna3">' + datos.username + '</td>'
                    + '<td class="columna4">' + datos.email + '</td>'
                    + '<td class="columna5">' + datos.address.street + ', ' + datos.address.suite + ', ' + datos.address.city + ', ' + datos.address.zipcode + '</td>'
                    + '<td class="columna6">' + datos.phone + '</td>'
                    + '<td class="columna7">' + datos.website + '</td>'
                    + '<td class="columna8">' + datos.company.name + ', ' + datos.company.catchPhrase + ', ' + datos.company.bs + '</td>';
                res.appendChild(fila);
            } else {

                alert("ID no encontrado");
            }
        }
    }
    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnBuscar").addEventListener("click", function () {
    var idBuscar = document.getElementById("txtBuscar").value;
    hacerpeticionPorId(idBuscar);
});

document.getElementById("btnCargar").addEventListener("click", function () {
    hacerpeticion();
});

document.getElementById("btnLimpiar").addEventListener("click", function () {
    let res = document.getElementById("lista");
    res.innerHTML = "";
});
